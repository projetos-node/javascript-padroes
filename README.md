# Javascript: Padrões de Projeto

Projeto do [curso](https://cursos.alura.com.br/course/javascript-es6-orientacao-a-objetos-parte-1) na plataforma Alura.

## Anotações

 - Programação orientada a objeto tem forte conexão entre dado e comportamento 
 - O construtor é chamado através do operador `new`
 - Nome de model/classe com letra maiúscula
 - Convenção de uso do underline `_`: propriedades só podem ser acessadas pelos métodos da própria classe
 - Encapsulamento: Métodos acessadores para ter acesos a dados "_somenteLeitura"
 - Sintaxe get: Quando a propriedade é um `getter` ela não pode receber valor. É um método que tem aparência de propriedade:
 ```
    get quantidade() {
        return this._quantidade;
    }
    ...
    var n = new Negociacao();
    n.quantidade = 100; // dont work
    n._quantidade = 100; // work
```
- `this`: variável implícita de acesso ao objeto através do constructor e métodos